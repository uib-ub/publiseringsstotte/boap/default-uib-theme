{**
* plugins/generic/embedHtmlArticleGalleyPlugin/display.tpl
*
* Copyright (c) 2014-2021 Simon Fraser University
* Copyright (c) 2003-2021 John Willinsky
* Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
*
* Embedded viewing of a HTML galley.
*}
{include file=$headerTemplatePath}
{include file=$breadcrumbsTemplatePath}
    <div id="htmlContainer">
                        {if !$isLatestPublication}
                                <div class="galley_view_notice">
                                        <div class="galley_view_notice_message" role="alert">
                                                {translate key="submission.outdatedVersion" datePublished=$galleyPublication->getData('datePublished')|date_format:$dateFormatLong urlRecentVersion=$articleUrl}
                                        </div>
                                 </div>
                        {/if}
                        {$html}
                        {* UZH CHANGE OJS-62 sui-generis script for numbering of paragraphs *}
                        <script>
                                $('.content p').each(function(){
                                        var nrid = $(this).attr('id');
                                        if (nrid) {
                                                var nrid2 = nrid.replace('nr-','');
                                                $(this).replaceWith('<p class="nr">'+nrid2+'</p>');
                                        }
                                });
                        </script>
                        {* END CHANGE OJS-62 *}

                        {* UZH CHANGE OJS-79 (GISo) script for responsive tables *}
                        <script>
                                $('table').wrap('<div style="overflow-x:auto;"></div>');
                        </script>
                        {* END UZH CHANGE OJS-79 *}

                        {* UZH CHANGE OJS-67 2019/03/08/mb display Licensing info *}
                        {* Licensing info *}

    {* Some $html fragments already includes Copyright info *}
    {if ($copyright || $licenseUrl) and !($html|strstr: "Copyright:") }
       <div class="item copyright">
                                        {if $licenseUrl}
                                                {if $ccLicenseBadge}
                                                        {if $copyrightHolder}
                                                                <p>{translate key="submission.copyrightStatement" copyrightHolder=$copyrightHolder copyrightYear=$copyrightYear}</p>
                                                        {/if}
                                                        {$ccLicenseBadge}
                                                {else}
                                                        <a href="{$licenseUrl|escape}" class="copyright">
                                                                {if $copyrightHolder}
                                                                        {translate key="submission.copyrightStatement" copyrightHolder=$copyrightHolder copyrightYear=$copyrightYear}
                                                                {else}
                                                                        {translate key="submission.license"}
                                                                {/if}
                                                        </a>
                                                {/if}
                                        {/if}
                                </div>
    {/if}
    {* END UZH CHANGE OJS-67 *}
</div>
{call_hook name="Templates::Common::Footer::PageFooter"}
{include file="frontend/components/footer.tpl"}
</body>
</html>