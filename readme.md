# UiB child theme

Child theme used for Bergen Open Access Publishing.

* See https://docs.pkp.sfu.ca/pkp-theming-guide/en/ for PKP theme documentation 
* See https://github.com/NateWr/defaultChild (full typed out example for default child theme) for specific implementations.


